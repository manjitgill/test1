import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	Snake	snake1, snake2; 
 
	@Before
	public void setUp() throws Exception {
		snake1 = new Snake("Peter", 10, "coffee");
		snake2 = new Snake("Takis", 80, "vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}
//TC1 Check if the snake is healthy
	@Test
	public void testisHealth() {
	//snake is healthy or not
		Boolean answer1 = snake1.isHealthy();
		if(answer1)
		{
			System.out.println("true");
		}
		else
		{
			System.out.println("False");
		}
		//test the output
		assertFalse("vegetables", answer1);
		
		Boolean answer2 = snake2.isHealthy();
		
		//test the output
		
		assertTrue("vegetables", answer2);
		if(answer2)
		{
			System.out.println("true");
		}
		else
		{
			System.out.println("False");
		}
		
	}
	//TC2 Check if the snake length is good
	
	@Test
	public void fitsinCage() {
		Boolean length1 = snake1.fitsInCage(50);
		assertTrue(length1);
		if(length1)
		{
			System.out.println("true");
		}
		else
		{
			System.out.println("False");
		
		}
		Boolean length2 = snake1.fitsInCage(90);
		assertTrue(length2);
		if(length2)
		{
			System.out.println("true");
		}
		else
		{
			System.out.println("False");
	}
		
		
		
	
	}
}
